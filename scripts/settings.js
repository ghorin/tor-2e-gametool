export class GametoolSettings {

    constructor() {
        this.gametool_options();
    }

    // ===================================================
    // OPTIONS FOR COMMUNITY and EYE AWARENESS 
    // ===================================================
    gametool_options() {
        game.settings.register('tor-2e-gametool', 'gametool-activate', {
            name: '### GAME TOOL ### \n ... Activate TOR2e Game Tool',
            config: true,
            type: Boolean,
            default: false
        });
        game.settings.register('tor-2e-gametool', 'gametool-posLeft', {
            name: '... Horizontal position (pixels from left)',
            config: true,
            type: Number,
            default: 500
        });
        game.settings.register('tor-2e-gametool', 'gametool-posTop', {
            name: '... Vertical position (pixels from top)',
            config: true,
            type: Number,
            default: 500
        });      
    }


}