export class GametoolFormApplication extends FormApplication {
    constructor(object, options) {
        super(object, options);
    }

    static get defaultOptions() {
        //return super.defaultOptions;
        let posTop = game.settings.get("tor-2e-gametool", "gametool-posTop");
        let posLeft = game.settings.get("tor-2e-gametool", "gametool-posLeft");

        return mergeObject(super.defaultOptions, {
            title: "In south Ered Luin",
            top: posTop,
            left: posLeft, 
            height: 360,
            width: 200,
            template: "modules/tor-2e-gametool/html/gametool.html",
            closeOnSubmit: false,
            submitOnClose: false,
            submitOnChange: false,
            resizable: false,
            minimizable: true,
            popOut: true,
            editable: true,
            shareable: false,
            classes: ["tor2e-gametool-window", game.system.id]
        });
    }

    getData(options = {}) {
        return super.getData().object; // the object from the constructor is where we are storing the data
    }

    activateListeners(html) {
        super.activateListeners(html);
    }

    async _updateObject(event, formData) {
        return;
    }
}