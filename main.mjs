import { GametoolSettings } from "./scripts/settings.js";
import { Gametool } from "./scripts/gametool.js";

class Tor2eGametool {
    constructor() {
        // ================
        // GAME TOOL
        // ================
        this.settings = new GametoolSettings();
        let settingActivateGametool = game.settings.get("tor-2e-gametool", "gametool-activate");
        if (settingActivateGametool) {
            this.gametool = new Gametool();
        }
    }
}

Hooks.on("ready", function() {
    game.tor2eGametool = new Tor2eGametool();
    console.log("=== Tor2eGametool : Ready ===");
});